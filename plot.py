from hdc_template import generateHDC
import matplotlib.pyplot as plt
from helper import centerAnglesWithY, radToDeg
from params import n_hdc, lam_hdc
from hdcAttractorConnectivity import HDCAttractorConnectivity
import numpy as np
from hdcTargetPeak import targetPeakDefault
from neuron import phi, r_m
import hdcOptimizeShiftLayers
import hdcOptimizeAttractor


def plotWeights():
    attrConn = HDCAttractorConnectivity(n_hdc, lam_hdc)
    X = range(-49, 50)
    w_HDC_HDC = [attrConn.connection(0, j % n_hdc) for j in X]
    w_HDC_SL = [attrConn.connection(0, j % n_hdc) * 0.5 for j in X]

    offset = 5
    strength = 1.0
    def peak_right(i, j):
        return strength * attrConn.connection((i + offset) % n_hdc, j)
    def peak_left(i, j):
        return strength * attrConn.connection((i - offset) % n_hdc, j)
    def conn_right(i, j):
        return peak_right(i, j) - peak_left(i, j)
    def conn_left(i, j):
        return peak_left(i, j) - peak_right(i, j)
    
    w_Sleft_HDC = [conn_left(0, j) for j in X]
    w_Sright_HDC = [conn_right(0, j) for j in X]

    plt.plot(X, w_HDC_HDC, label="HDC -> HDC")
    plt.plot(X, w_HDC_SL, label="HDC -> shift layers")
    plt.plot(X, w_Sleft_HDC, label="shift left -> HDC")
    plt.plot(X, w_Sright_HDC, label="shift right -> HDC")

    plt.legend()
    plt.hlines(0.0, -49.0, 49.0, colors="k", linestyles="--", linewidth=1)
    plt.xlabel("distance in intervals between neurons")
    plt.ylabel("synaptic weight")
    plt.show()

