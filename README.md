Installation:
 * Required software:
   - Python 3 (Python 2 may work as well)
   - NumPy (pip install numpy)
   - SciPy (pip install scipy)
   - PyBullet (pip install pybullet)
   - tqdm (pip install tqdm)
 * Recommended software:
   - PyCuda for GPU acceleration (https://wiki.tiker.net/PyCuda/Installation/) (if a GPU with CUDA support is available)
 * Set the backend (NumPy or CUDA) in network.py

Scripts:
 - controller.py: run the robot simulation. Environment / dataset and simulation parameters can be set on the top of that file
 - generate_avs_from_sim.py: run pybullet and save all the changes in angle for every timestep in a file readable by controller.py
 - plotting.py: generate most of the figures in the thesis, the ones belonging to the simulation in Chapter 3 are generated with controller.py

Functionality:
 - hdc_template.py: generate HDC network according to parameters defined in params.py
 - hdcAttractorConnectivity: generate connections inside the HDC attractor network given the parameter lambda
 - hdcNetwork.py: generate the HDC network given weight functions, also contains the function for initializing the HDC network
 - hdcOptimizeAttractor.py: contains the procedure used to find the value for lambda yielding the best weight function
 - hdcOptimizeShiftLayers.py: contains the procedure used to generate the plots for shifting (Figures 2.11, 2.12) as well as finding the factor for the angular velocity -> stimulus function (Equation 2.11)
 - helper.py: helper functions (decode attractor network, distance between angles, ...)
 - import_kitti.py: generate the change in angle for every timestep from the kitti (http://www.cvlibs.net/datasets/kitti/) odometry dataset, angular velocities are calculated from the orientation ground truth data. 
 - network.py: proxy for selecting one of the network backends:
   - network_cuda.py
   - network_numpy.py
 - neuron.py: neuron model parameters
 - params.py: parameters for the network (number of neurons, lambda)
 - polarPlotter.py: live visualization of the HDC network, shown in Figure 3.2
 - pybullet_environment.py: pybullet environment class, braitenberg controller
 - stresstest.py: performance testing