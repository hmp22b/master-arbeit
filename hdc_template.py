from network import NetworkTopology
import hdcNetwork
import numpy as np
from hdcAttractorConnectivity import HDCAttractorConnectivity
from params import n_hdc, lam_hdc

def norm_shift(i, j, n, amp, sigma):
    if abs(i - j) < float(n) / 2.0:
        # shortest path between i and j doesn't pass 0
        dist = i - j
    else:
        # shortest path between i and j passes 0
        dist = i - (j - n)
        if i > j:
            dist = (i - n) - j
        elif i < j:
            dist = i + (n - j)
    x = (dist / float(n)) * 2 * np.pi
    s = 2*(sigma**2)
    val = amp * (np.sqrt(2*np.e) / np.sqrt(s))*x*np.exp(-(1/s)*(x**2))
    return val

# generates hdc network instance with parameters as defined in the thesis
def generateHDC():
    # calculate weights
    attrConn = HDCAttractorConnectivity(n_hdc, lam_hdc)

    # initialize hdc
    topo = NetworkTopology()
    hdcNetwork.addHDCAttractor(topo, n_hdc, attrConn.connection)


    # add shift layers
    # HDC -> shift layers
    connHDCS = lambda i, j : attrConn.connection(i, j) * 0.5

    # shift layers -> HDC
    # offset in neurons
    offset = 5
    strength = 1.0
    def peak_right(to, fr):
        return strength * attrConn.connection(to, (fr + offset) % n_hdc)
        # return 0
    def peak_left(to, fr):
        return strength * attrConn.connection(to, (fr - offset) % n_hdc)
        # return 0
    hdcNetwork.addHDCShiftLayers(topo, n_hdc, connHDCS, lambda i, j : peak_right(i, j) - peak_left(i, j), connHDCS, lambda i, j : peak_left(i, j) - peak_right(i, j))

    # make instance
    hdc = topo.makeInstance()

    # initialize
    hdcNetwork.initializeHDC(hdc, 0.0)
    return hdc