from hdc_template import generateHDC
import matplotlib.pyplot as plt
from helper import centerAnglesWithY, radToDeg
from params import n_hdc, lam_hdc
from hdcAttractorConnectivity import HDCAttractorConnectivity
import numpy as np
from hdcTargetPeak import targetPeakDefault
from neuron import phi, r_m
import hdcOptimizeShiftLayers
import hdcOptimizeAttractor

def plotHDCSL():
    hdc = generateHDC()
    rates_shiftl = hdc.getLayer('hdc_shift_left')
    rates_shiftr = hdc.getLayer('hdc_shift_right')
    rates_hdc = hdc.getLayer('hdc_attractor')

    X = np.linspace(0.0, 2*np.pi * ((n_hdc-1) / n_hdc), n_hdc)
    A, B = centerAnglesWithY(X, rates_shiftl)
    A, C = centerAnglesWithY(X, rates_shiftr)
    A, D = centerAnglesWithY(X, rates_hdc)
    plt.plot(radToDeg(A), D, label='HDC Attractor')
    plt.plot(radToDeg(A), B, label='shift layers')
    plt.legend()
    plt.xlabel('preferred direction (deg)')
    plt.ylabel('firing rate (Hz)')
    plt.show()

def plotWeights():
    attrConn = HDCAttractorConnectivity(n_hdc, lam_hdc)
    X = range(-49, 50)
    w_HDC_HDC = [attrConn.connection(0, j % n_hdc) for j in X]
    w_HDC_SL = [attrConn.connection(0, j % n_hdc) * 0.5 for j in X]

    offset = 5
    strength = 1.0
    def peak_right(i, j):
        return strength * attrConn.connection((i + offset) % n_hdc, j)
    def peak_left(i, j):
        return strength * attrConn.connection((i - offset) % n_hdc, j)
    def conn_right(i, j):
        return peak_right(i, j) - peak_left(i, j)
    def conn_left(i, j):
        return peak_left(i, j) - peak_right(i, j)
    
    w_Sleft_HDC = [conn_left(0, j) for j in X]
    w_Sright_HDC = [conn_right(0, j) for j in X]

    plt.plot(X, w_HDC_HDC, label="HDC -> HDC")
    plt.plot(X, w_HDC_SL, label="HDC -> shift layers")
    plt.plot(X, w_Sleft_HDC, label="shift left -> HDC")
    plt.plot(X, w_Sright_HDC, label="shift right -> HDC")

    plt.legend()
    plt.hlines(0.0, -49.0, 49.0, colors="k", linestyles="--", linewidth=1)
    plt.xlabel("distance in intervals between neurons")
    plt.ylabel("synaptic weight")
    plt.show()

def plotTuningCurves():
    r2d = 360 / (2*np.pi)
    X_neuron = range(100)
    X_ang = [(i * 2 * np.pi) / 100 for i in X_neuron]
    Y_peak = [targetPeakDefault(x) for x in X_ang]
    Y_neg90 = [targetPeakDefault( (x + (0.5 * np.pi)) % (2*np.pi)) for x in X_ang]
    Y_pos90 = [targetPeakDefault( (x - (0.25 * np.pi)) % (2*np.pi)) for x in X_ang]
    Y_neg225 = [targetPeakDefault( (x + (0.125 * np.pi)) % (2*np.pi)) for x in X_ang]

    X = X_ang

    X, Y_pos90 = centerAnglesWithY(X_ang, Y_pos90)
    X, Y_neg90 = centerAnglesWithY(X_ang, Y_neg90)
    X, Y_neg225 = centerAnglesWithY(X_ang, Y_neg225)
    X, Y_peak = centerAnglesWithY(X_ang, Y_peak)
    plt.plot(radToDeg(X), Y_pos90, label="tuning curve, preferred direction $45^\circ$", color="g")
    plt.plot(radToDeg(X), Y_neg90, label="tuning curve, preferred direction $-90^\circ$", color="tab:orange")
    plt.plot(radToDeg(X), Y_neg225, label="tuning curve, preferred direction $-22.5^\circ$", color="r")


    plt.scatter([0, 45], [targetPeakDefault(0.25*np.pi)]*2, color="g")
    plt.scatter([0, -90], [targetPeakDefault(0.5*np.pi)]*2, color="tab:orange")
    plt.scatter([0, -22.5], [targetPeakDefault(0.125*np.pi)]*2, color="r")


    plt.plot([0, 45], [targetPeakDefault(0.25*np.pi)]*2, color="g", linestyle="dotted")
    plt.plot([0, -90], [targetPeakDefault(0.5*np.pi)]*2, color="orange", linestyle="dotted")
    plt.plot([0, -22.5], [targetPeakDefault(0.125*np.pi)]*2, color="r", linestyle="dotted")

    plt.plot(radToDeg(X), Y_peak, label="activity peak", color="k", linestyle="--")

    plt.vlines([-90, -22.5, 0, 45], 0, targetPeakDefault(0.0), color="k", linestyle="dotted", linewidth=1.0)
    plt.xlim(-100, 60)
    plt.ylim(0.0, 100)
    plt.xticks([-90, -22.5, 0, 45])
    plt.xlabel("direction (deg)")
    plt.ylabel("firing rate (Hz)")

    plt.legend()
    plt.show()

def plotPhi():
    X = np.linspace(-5.0, 12.0, 1000)
    plt.plot(X, [phi(x) for x in X])
    plt.xlim(-5.0, 12.0)
    plt.ylim(0.0, r_m)
    plt.plot([-5.0, 12.0], [phi(0.0), phi(0.0)], linestyle="dotted", color="k")
    plt.plot([0.0, 0.0], [0.0, r_m], linestyle="dotted", color="k")
    plt.xlabel("$x$")
    plt.ylabel("$\phi(x)$")
    plt.show()

def plotMultipleActivityPeaks():
    pass

if __name__ == "__main__":
    print("Available Figures:")
    print("2.1:  Function phi")
    print("2.2:  Tuning curves vs. activity profile")
    print("2.4:  Activity peaks for differend values for lambda")
    print("2.5:  Error for different values for lambda (logarithmic)")
    print("2.6:  Error for different values for lambda (linear)")
    print("2.7:  Activity peak vs. target activity peak")
    print("2.8:  Weights inside the attractor network")
    print("2.9:  Activity peaks HDC and shift layers")
    print("2.10: All weights")
    print("2.11: Decoded direction over time during shifting")
    print("2.12: Angular velocities reached with different stimuli")
    print("------------------ NOTES ------------------")
    print("Figure 2.3 is made directly in LaTEX and can't be plotted here.")
    print("All Figures in Chapter 3 'Results and Discussion' are plotted with controller.py")
    print("-------------------------------------------")
    terminate = False
    while not terminate:
        print("Enter figure number (e.g. '2.1') to plot it or close with 'exit': ", end="")
        fig_name = input()
        if fig_name == "2.1":
            plotPhi()
        elif fig_name == "2.2":
            plotTuningCurves()
        elif fig_name == "2.4":
            hdcOptimizeAttractor.plotActivityPeaks()
        elif fig_name == "2.5":
            print("The figure in the thesis is plotted with 10000 lambda values logarithmically distributed from 10^-10 to 10^10. This may take several hours.")
            print("Press Enter to plot the same as in the thesis or enter custom parameters in the form 'f t r' to plot with r values from 10^f to 10^t: ", end="")
            params = input().split()
            if params == []:
                params = ["-10", "10", "10000"]
            params = list(map(int, params))
            hdcOptimizeAttractor.plotErrorLambda(np.logspace(params[0], params[1], params[2]), True)
        elif fig_name == "2.6":
            print("The figure in the thesis is plotted with 10000 lambda values linearly distributed from 25000 to 26000. This may take several hours.")
            print("Press Enter to plot the same as in the thesis or enter custom parameters in the form 'f t r' to plot with r values from f to t: ", end="")
            params = input().split()
            if params == []:
                params = ["25000", "26000", "10000"]
            params = list(map(int, params))
            hdcOptimizeAttractor.plotErrorLambda(np.linspace(params[0], params[1], params[2]), False)
        elif fig_name == "2.7":
            hdcOptimizeAttractor.plotSinglePeak()
        elif fig_name == "2.8":
            hdcOptimizeAttractor.plotWeightFunction()
        elif fig_name == "2.9":
            plotHDCSL()
        elif fig_name == "2.10":
            plotWeights()
        elif fig_name == "2.11":
            hdcOptimizeShiftLayers.testStimuli(plots=["dir_over_time"], stims=[0.0, 0.01, 0.1, 0.5])
        elif fig_name == "2.12":
            hdcOptimizeShiftLayers.testStimuli(plots=["av_over_stim"])
        elif fig_name == "exit":
            terminate = True
        else:
            print("Figure '{}' unknown, exit with 'exit'".format(fig_name))
