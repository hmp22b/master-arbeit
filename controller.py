from bullet_reset import pionnerWolrd
import time
import numpy as np
import math
from tqdm import tqdm
import hdcNetwork
from hdcAttractorConnectivity import HDCAttractorConnectivity
import matplotlib.pyplot as plt
from polarPlotter import PolarPlotter
from gc_ploter import GCploter
import helper
from params import n_hdc, weight_av_stim, weight_gc_stim
from hdc_template import generateHDC
from scipy.stats import pearsonr
import random
import pickle
from gc_template import generateGC
######## for running from data ########
run_from_data = True
# thetas_file is a pickle file containing a tuple (thetas, t_episode, dt_robot)
# with thetas an array of the change in angle during every timestep
thetas_file = "data/thetas_pybullet_maze.p"
#######################################
######## rotation of the map ##########
rotation=0
######## for running pybullet #########
test=False
# total episode time in seconds
if not test:
    t_episode = 8000
else:
    t_episode=500
# minimum neuron model timestep
dt_neuron_min = 0.0025
# robot timestep
dt_robot = 0.05
# simulation environment, available models: "maze", "plus"
env_model = "maze"
# the simulation environment window can be turned off, speeds up the simulation significantly
env_visualize = True
#######################################


###### matplotlib visualization #######
rtplot = False
plotfps = 2.5
#######################################
####### noisy angular velocity ########
use_noisy_av = False

# gaussian noise
# relative standard deviation (standard deviation = rel. sd * av)
noisy_av_rel_sd = 0.0
# absolute standard deviation (deg)
noisy_av_abs_sd = 0.0

# noise spikes
# average noise spike frequency in Hz
noisy_av_spike_frequency = 0.0
# average magnitude in deg/s
noisy_av_spike_magnitude = 0.0
# standard deviation in deg/s
noisy_av_spike_sd = 0.0

# noise oscillation
noisy_av_osc_frequency = 0.0
noisy_av_osc_magnitude = 0.0
noisy_av_osc_phase = 0.0
#######################################

# Initialize environment
if not run_from_data:
    env = pionnerWolrd(1/dt_robot,env_visualize)
    env.reset()
else:
    # with open(thetas_file, "rb") as fl:
    #     (thetas_in, t_episode, dt_robot) = pickle.load(fl)
    #     fl.close()
    with open('data_env_11_0.05_big.pkl','rb') as out_data:
        Theta=pickle.load(out_data)
        # print('Theta',Theta)
        V=pickle.load(out_data)
        # print('V',V)
        Position=pickle.load(out_data)
        # print('Position',Position)

# find dt_neuron < dt_neuron_min = dt_robot / timesteps_neuron with timesteps_neuron integer
timesteps_neuron = math.ceil(dt_robot/dt_neuron_min)
print(timesteps_neuron)
dt_neuron = dt_robot / timesteps_neuron
# init plotter
nextplot = time.time()
if rtplot:
    if env_model == "maze":
        plotter = PolarPlotter(n_hdc, 0.5 * np.pi, False)
    else:
        plotter = PolarPlotter(n_hdc, 0.0, False)
    gc_ploter=GCploter()
# init gc ploter

# init HDC
hdc = generateHDC()
# init GC
gc = generateGC()

# list for HDC
realDir = 0.0
avs = []
errs = []
errs_signed = []
thetas = []
netTimes = []
robotTimes = []
plotTimes = []
transferTimes = []
decodeTimes = []
errs_noisy_signed = []
noisyDir = 0.0

# list for gc
location = []
firing_list = []
firing_location = []
decodeTimes_gc = []

t_before = time.time()
t_ctr = 0
Acti=[]
gc_decode_x=[]
gc_decode_y=[]
if test:
    v_test=0
for t in tqdm(np.arange(0.0, t_episode, dt_robot)):
    def getStimL(ahv):
        if ahv < 0.0:
            return 0.0
        else:
            return ahv * weight_av_stim

    def getStimR(ahv):
        if ahv > 0.0:
            return 0.0
        else:
            return - ahv * weight_av_stim

    def getStimup(y):
        if y < 0:
            return 0
        else:
            return y*weight_gc_stim

    def getStimdown(y):
        if y > 0:
            return 0
        else:
            return -y*weight_gc_stim

    def getStimleft(x):
        if x > 0:
            return 0
        else:
            return -x*weight_gc_stim

    def getStimright(x):
        if x < 0:
            return 0
        else:
            return x*weight_gc_stim

    def getNoisyTheta(theta):
        noisy_theta = theta
        # gaussian noise
        if noisy_av_rel_sd != 0.0:
            noisy_theta = random.gauss(noisy_theta, noisy_av_rel_sd * theta)
        if noisy_av_abs_sd != 0.0:
            noisy_theta = random.gauss(
                noisy_theta, noisy_av_abs_sd * dt_robot * (1/r2d))
        # noise spikes
        if noisy_av_spike_frequency != 0.0:
            # simplified, should actually use poisson distribution
            probability = noisy_av_spike_frequency * dt_robot
            if random.random() < probability:
                deviation = random.gauss(
                    noisy_av_spike_magnitude * dt_robot * (1/r2d), noisy_av_spike_sd * dt_robot * (1/r2d))
                print(deviation)
                if random.random() < 0.5:
                    noisy_theta = noisy_theta + deviation
                else:
                    noisy_theta = noisy_theta - deviation
        # noise oscillation
        if noisy_av_osc_magnitude != 0.0:
            noisy_theta += noisy_av_osc_magnitude * dt_robot * \
                (1/r2d) * np.sin(noisy_av_osc_phase + noisy_av_osc_frequency * t)
        return noisy_theta
    # rad to deg factor
    r2d = (360 / (2*np.pi))

    # robot simulation step
    action = []
    beforeStep = time.time()
    if not run_from_data:
        theta, v,position = env.step()
        # plus velocity in x and y direction
    else:
        theta = Theta[t_ctr]
        v=V[t_ctr]
        position=Position[t_ctr]
    afterStep = time.time()
    robotTimes.append((afterStep - beforeStep))
    thetas.append(theta)

    # current is calculated from angular velocity
    angVelocity = theta * (1.0/dt_robot)
    # add noise
    noisy_theta = getNoisyTheta(theta)
    av_net = noisy_theta * (1.0/dt_robot) if use_noisy_av else angVelocity
    avs.append(angVelocity)
    stimL = getStimL(av_net)
    stimR = getStimR(av_net)
    # print(av_net, stimL, stimR)

    

    beforeStep = time.time()
    hdc.setStimulus('hdc_shift_left', lambda _: stimL)
    hdc.setStimulus('hdc_shift_right', lambda _: stimR)
    # HDC step
    hdc.step(dt_neuron, numsteps=timesteps_neuron)
    afterStep = time.time()
    netTimes.append((afterStep - beforeStep) / timesteps_neuron)
    #get the hdc rate
    rates_hdc = list(hdc.getLayer('hdc_attractor'))
    rates_sl = list(hdc.getLayer('hdc_shift_left'))
    rates_sr = list(hdc.getLayer('hdc_shift_right'))
    # print('rates_hdc',rates_hdc)
    # print('type',type(rates_hdc))

    # decode direction, calculate errors for HDC
    beforeStep = time.time()
    decodedDir = helper.decodeAttractor(rates_hdc)
    realDir = (realDir + theta) % (2 * np.pi)
    noisyDir = (noisyDir + noisy_theta) % (2 * np.pi)
    err_noisy_signed_rad = helper.angleDist(realDir, noisyDir)
    errs_noisy_signed.append(r2d * err_noisy_signed_rad)
    err_signed_rad = helper.angleDist(realDir, decodedDir)
    errs_signed.append(r2d * err_signed_rad)
    errs.append(abs(r2d * err_signed_rad))
    afterStep = time.time()
    decodeTimes.append(afterStep - beforeStep)
    # print('decodedDir',decodedDir*r2d)
    # simulate network GC

        #get the velocity from env
    v_x = v[0]
    v_y = v[1]
    v_x=np.cos(rotation)*v_x-np.sin(rotation)*v_y
    v_y=np.sin(rotation)*v_x+np.cos(rotation)*v_y
   
    print('v_x and v_y',v_x,v_y)


       
       
       # test for settle
    if test:
        stimleft=0
        stimright=0.05
        stimup=0
        stimdown=0
    else:
        stimup = getStimup(v_y)
        stimdown = getStimdown(v_y)
        stimleft = getStimleft(v_x)
        stimright = getStimright(v_x)
        
    # print('stimup',stimup)
    print('stimdown',stimdown)

    print('stimright',stimright)   
    
    #set the stimulus of gc
    gc.setStimulus('gc_shift_left', lambda _: stimleft)
    gc.setStimulus('gc_shift_right', lambda _: stimright)
    gc.setStimulus('gc_shift_up', lambda _: stimup)
    gc.setStimulus('gc_shift_down', lambda _: stimdown)
    print('stimleft',stimleft)
    #gc step
    gc.step(dt_neuron, numsteps=timesteps_neuron)
    
    
    #get the rates from gc neurons
    rates_gc = list(gc.getLayer('gc_attractor'))
    rates_gl = list(gc.getLayer('gc_shift_left'))
    rates_gr = list(gc.getLayer('gc_shift_right'))
    rates_gu = list(gc.getLayer('gc_shift_up'))
    rates_gd = list(gc.getLayer('gc_shift_down'))
    # print('gc_active',rates_gc)
    # print('gl_active',rates_gl)
    # decode for gc and save the data
    beforeStep = time.time()
    decoded_x, decoded_y = helper.decodeGC(rates_gc)
    firing_index, firing_x, firing_y = helper.firing_index(rates_gc)
    xgc_angle=helper.decodeAttractor_x(rates_gc)*r2d
    ygc_angle=helper.decodeAttractor_y(rates_gc)*r2d
    # print('x,y=', decoded_x,decoded_y)
    # print('ygc_angle',ygc_angle)
    print('xgc_angle',xgc_angle)
    # print(rates_gc)
    decodeTimes_gc.append(afterStep - beforeStep)
    # plotting
    if time.time() > nextplot and rtplot:
        nextplot += 1.0 / plotfps
        beforeStep = time.time()
        plotter.plot(rates_hdc, rates_sl, rates_sr,
                     stimL, stimR, realDir, decodedDir)
        afterStep = time.time()
        plotTimes.append((afterStep - beforeStep))
        gc_ploter.plot_active(rates_gc)
    afterStep = time.time()

    # print('atvie of GC',rates_gc)
    # print('atvie of shift layer',rates_gl)
    Acti.append(rates_gc)
    gc_decode_y.append(ygc_angle)
    gc_decode_x.append(xgc_angle)
    t_ctr += 1
    # if t_ctr>1500:
    #     gc_ploter.plot_active(rates_gc)
    # gc_ploter.plot_active(rates_gc)
    if test:
        v_test+=0.00005
    if rtplot:
        time.sleep(0.05)
# save the acti of gc

if test:
    with open('avti_1.6_big_test_x.pkl','wb') as in_data:
        pickle.dump(Acti,in_data,pickle.HIGHEST_PROTOCOL)
    with open('decode_gc_1.6_x_big_test_x.pkl','wb') as in_data2:
        pickle.dump(gc_decode_x,in_data2,pickle.HIGHEST_PROTOCOL)
    with open('decode_gc_1.6_y_big_test_x.pkl','wb') as in_data2:
        pickle.dump(gc_decode_y,in_data2,pickle.HIGHEST_PROTOCOL)
else:
    with open('avti_1.8_big.pkl','wb') as in_data:
        pickle.dump(Acti,in_data,pickle.HIGHEST_PROTOCOL)

    with open('decode_gc_1.8_x_big.pkl','wb') as in_data2:
        pickle.dump(gc_decode_x,in_data2,pickle.HIGHEST_PROTOCOL)
    with open('decode_gc_1.8_y_big.pkl','wb') as in_data2:
        pickle.dump(gc_decode_y,in_data2,pickle.HIGHEST_PROTOCOL)
# final calculations
t_total = time.time() - t_before
X = np.arange(0.0, t_episode, dt_robot)
cahv = [avs[i] - avs[i - 1] if i > 0 else avs[0] for i in range(len(avs))]
cerr = [errs_signed[i] - errs_signed[i - 1] if i > 0 else errs_signed[0]
        for i in range(len(errs_signed))]
corr, _ = pearsonr(cahv, cerr)

# error noisy integration vs. noisy HDC
if use_noisy_av:
    plt.plot(X, errs_noisy_signed, label="Noisy integration")
    plt.plot(X, errs_signed, label="Noisy HDC")
    plt.plot([0.0, t_episode], [0.0, 0.0], linestyle="dotted", color="k")
    plt.xlabel("time (s)")
    plt.ylabel("error (deg)")
    plt.legend()
    plt.show()

# print results
print("\n\n\n")
print("############### Begin Simulation results ###############")
# performance tracking
print("Total time (real): {:.2f} s, Total time (simulated): {:.2f} s, simulation speed: {:.2f}*RT".format(
    t_total, t_episode, t_episode / t_total))
print("Average step time network:  {:.4f} ms; {} it/s possible".format(
    1000.0 * np.mean(netTimes), 1.0/np.mean(netTimes)))
print("Average step time robot:    {:.4f} ms; {} it/s possible".format(
    1000.0 * np.mean(robotTimes), 1.0/np.mean(robotTimes)))
if rtplot:
    print("Average step time plotting: {:.4f} ms; {} it/s possible".format(
        1000.0 * np.mean(plotTimes), 1.0/np.mean(plotTimes)))
time_coverage = 0.0
print("Average time decoding:      {:.4f} ms; {} it/s possible".format(
    1000.0 * np.mean(decodeTimes), 1.0/np.mean(decodeTimes)))
print("Steps done network:  {}; Time: {:.3f} s; {:.2f}% of total time".format(len(X) * timesteps_neuron, len(X)
                                                                              * timesteps_neuron * np.mean(netTimes), 100 * len(X) * timesteps_neuron * np.mean(netTimes) / t_total))
time_coverage += 100 * len(X) * timesteps_neuron * np.mean(netTimes) / t_total
print("Steps done robot:    {}; Time: {:.3f} s; {:.2f}% of total time".format(
    len(X), len(X) * np.mean(robotTimes), 100 * len(X) * np.mean(robotTimes) / t_total))
time_coverage += 100 * len(X) * np.mean(robotTimes) / t_total
if rtplot:
    print("Steps done plotting: {}; Time: {:.3f} s; {:.2f}% of total time".format(int(t_episode / plotfps),
                                                                                  int(t_episode / plotfps) * np.mean(plotTimes), 100 * int(t_episode / plotfps) * np.mean(plotTimes) / t_total))
    time_coverage += 100 * int(t_episode / plotfps) * \
        np.mean(plotTimes) / t_total
print("Steps done decoding: {}; Time: {:.3f} s; {:.2f}% of total time".format(len(
    X), len(X) * np.mean(decodeTimes), 100 * len(X) * np.mean(decodeTimes) / t_total))
time_coverage += 100 * len(X) * np.mean(decodeTimes) / t_total
print("Time covered by the listed operations: {:.3f}%".format(time_coverage))
print("maximum angular velocity: {:.4f} deg/s".format(max(avs) * r2d))
print("average angular velocity: {:.4f} deg/s".format(
    sum([r2d * (x / len(avs)) for x in avs])))
print("median angular velocity:  {:.4f} deg/s".format(np.median(avs)))
print("maximum error: {:.4f} deg".format(max(errs)))
print("average error: {:.4f} deg".format(np.mean(errs)))
print("median error:  {:.4f} deg".format(np.median(errs)))
print("################ End Simulation results ################")
print("\n\n\n")

# close real-time plot
plt.close()
plt.ioff()

# plot error and angular velocity
fig, ax1 = plt.subplots()
# ax1.set_xlim(200, 375)
ax1.set_xlabel("time (s)")
ax1.set_ylabel("error (deg)")
ax1.set_ylim(-1.6, 1.6)
ax1.plot(X, errs_signed, color="tab:blue")
ax1.tick_params(axis="y", labelcolor="tab:blue")
ax2 = ax1.twinx()
ax2.set_ylabel("angular velocity (deg/s)")
ax2.set_ylim(-50, 50)
ax2.plot(X, [x * r2d for x in avs], color="tab:orange")
ax2.tick_params(axis="y", labelcolor="tab:orange")
ax1.plot([0.0, t_episode], [0.0, 0.0], linestyle="dotted", color="k")
fig.tight_layout()
plt.show()

# plot only error
plt.xlabel("time (s)")
plt.ylabel("error (deg)")
plt.ylim(-1.6, 1.6)
plt.xlim(0.0, t_episode)
plt.plot(X, errs_signed)
plt.plot([0.0, t_episode], [0.0, 0.0], linestyle="dotted", color="k")
plt.show()

# plot only angular velocity
plt.xlabel("time (s)")
plt.ylabel("angular velocity (deg/s)")
plt.ylim(-50, 50)
plt.xlim(0.0, t_episode)
plt.plot(X, [x * r2d for x in avs])
plt.plot([0.0, t_episode], [0.0, 0.0], linestyle="dotted", color="k")
plt.show()

# plot total rotation
totalMovements = [0.0] * len(thetas)
for i in range(1, len(avs)):
    totalMovements[i] = totalMovements[i-1] + abs(r2d * thetas[i - 1])
plt.plot(X, totalMovements)
plt.xlabel("time (s)")
plt.ylabel("total rotation (deg)")
plt.show()

# plot relative error
# begin after 20%
begin_relerror = int(0.2 * len(X))
plt.plot(X[begin_relerror:len(X)], [100 * errs[i] / totalMovements[i]
                                    for i in range(begin_relerror, len(errs))])
plt.xlabel("time (s)")
plt.ylabel("relative error (%)")
plt.show()

# plot change in angular velocity vs. change in error
plt.scatter(cahv, cerr)
plt.plot([min(cahv), max(cahv)], [corr * min(cahv), corr * max(cahv)],
         label="linear approximation with slope {:.2f}".format(corr), color="tab:red")
plt.legend()
plt.xlabel("change in angular velocity (deg/s)")
plt.ylabel("change in error (deg)")
plt.show()
if not run_from_data:
    env.close()
