import numpy as np
import math
# re-implemented from Zhang 1995


def gctargetPeak(p,center=0):
    def volt(i, j):
        d = math.sqrt(3.0)/2.0

        def cartesian(i):
            return (i % 10, int(i/10 % 9))

        def c(x, y):
            return (x-0.5)/10, d*(y-0.5)/9

        def euclidean_length(x, y):
            return math.sqrt(x**2+y**2)

        def disttri(i, j):
            sj = [(0.0, 0.0), (-0.5, d), (-0.5, -d), (0.5, d),
                    (0.5, -d), (-1.0, 0.0), (1.0, 0.0)]
            xi, yi = cartesian(i)
            xi, yi = c(xi, yi)
            xj, yj = cartesian(j)
            xj, yj = c(xj, yj)
            return min([euclidean_length(xi-xj+xs, yi-yj+ys) for xs, ys in sj])

        return 69*np.exp(-disttri(i, j)**2/0.15**2)+2.72
    
    return volt(center, p)


def gctargetPeakDefault(p,center=0):
    def volt(i, j):
        d = math.sqrt(3.0)/2.0

        def cartesian(i):
            return (i % 10, int(i/10 % 9))

        def c(x, y):
            return (x-0.5)/10, d*(y-0.5)/9

        def euclidean_length(x, y):
            return math.sqrt(x**2+y**2)

        def disttri(i, j):
            sj = [(0.0, 0.0), (-0.5, d), (-0.5, -d), (0.5, d),
                    (0.5, -d), (-1.0, 0.0), (1.0, 0.0)]
            xi, yi = cartesian(i)
            xi, yi = c(xi, yi)
            xj, yj = cartesian(j)
            xj, yj = c(xj, yj)
            return min([euclidean_length(xi-xj+xs, yi-yj+ys) for xs, ys in sj])

        return cartesian(j),72*np.exp(-disttri(i, j)**2/0.16**2)+2.72
    
    return volt(center, p)
