import pickle
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pylab
import helper
from scipy.optimize import curve_fit
test=False
activ_test=False
use_max=True
env_l=1.6
env_w=1.6
dt=0.05
if not test:
     with open('avti_1.8_big.pkl','rb') as out_data:
          active=pickle.load(out_data)
     with open('decode_gc_1.8_x_big.pkl','rb') as out_data:
          angle_x=pickle.load(out_data)
     with open('decode_gc_1.8_y_big.pkl','rb') as out_data:
          angle=pickle.load(out_data)
     with open('data_env_11_0.05_big.pkl','rb') as out_data:
          Theta=pickle.load(out_data)
          V=pickle.load(out_data)
          Position=pickle.load(out_data)

if test:
     # with open('decode_gc_1.6_x_big_test_-x.pkl','rb') as out_data:
     #      angle_x1=pickle.load(out_data)
     with open('decode_gc_1.6_x_big_test_x.pkl','rb') as out_data:
          angle_x2=pickle.load(out_data)
     with open('data_env_11_0.05_big.pkl','rb') as out_data:
          Theta=pickle.load(out_data)
          print('Theta',len(Theta))
          V=pickle.load(out_data)
          # print('V',V)
          print('max_V',max(map(max,V)))
          Position=pickle.load(out_data)
          print('max_POS',max(map(max,Position)))
     with open('avti_1.6_big_test_x.pkl','rb') as out_data:
          active=pickle.load(out_data)     
     



if test:
     T_end=500
     T=np.arange(0.05,T_end,0.05)
     delta=[]
     fig,ax=plt.subplots()
     # derivert_X1=[(angle_x1[i+1]-angle_x1[i]) for i in range(len(angle_x1)-1)]
     derivert_X2=[(angle_x2[i+1]-angle_x2[i]) for i in range(len(angle_x2)-1)]
     end=0.5
     X=np.arange(0.00005,end,0.00005)
     for i in range(len(derivert_X2)):
          # if derivert_X1[i]<-100:
          #      derivert_X1[i]+=360
          # if derivert_X1[i]>100:
          #      derivert_X1[i]-=360
          if derivert_X2[i]<-100:
               derivert_X2[i]+=360
          if derivert_X2[i]>100:
               derivert_X2[i]-=360  
     for j in range(len(active)-1):
          avtive_now=active[j+1]
          avtive_before=active[j]
          delta.append(sum([abs(avtive_now[i] - avtive_before[i]) for i in range(20*18)])) 
     # derivert_X1=[abs(i)/360/dt for i in derivert_X1]
     derivert_X2=[abs(i)/360/dt for i in derivert_X2]
     X_li=X[:5000]
     derivert_X2_li=derivert_X2[:5000]
     Z=np.polyfit(X_li,derivert_X2_li,1 )
     p = np.poly1d(Z)
     print(p)
     ax.plot(X,derivert_X2,label='Velocity of Peak')
     pylab.plot(X, p(X),label='V =-9.1e-8s + 0.2326')
     # ax.plot(X,derivert_X2)
     # ax.plot(T,delta)
     ax.set_ylabel('Velocity of Peak')
     ax.set_xlabel('Stimulus')
     # ax.set_ylabel('Error(Hz)')
     # ax.set_xlabel('Time(s)')
     plt.legend(loc=1)
     plt.show()
     fig.savefig('0.5.pdf',dpi=600,format='pdf')
     # for i in range(int(len(X)/1000)):
     #      X_lim=X[:(i+1)*1000]
     #      derivert_X_lim=derivert_X[:(i+1)*1000]
     #      Z,diff,_,_,_=np.polyfit(X_lim, derivert_X_lim,1,full=True )
     #      diff=diff/((i+1)*1000)
     #      print('diff',diff)
     #      print('COF',Z)
     # p = np.poly1d(Z)
     # pylab.plot(X, p(X),label=f'y = {Z[0]}x + {Z[1]}')
     # ax.plot(X,derivert_X)
     # plt.show()
elif not activ_test:
     neuron=130
     ative_neuron=[active[i][neuron] for i in range(int(len(active)))]
     X_or=[Position[i][0] for i in range(int(len(active)))]
     Y_or=[Position[i][1] for i in range(int(len(active)))]
     if use_max:
          X=[Position[i][0] for i in range(int(len(active))) if ative_neuron[i]==max(active[i])]
          Y=[Position[i][1] for i in range(int(len(active))) if ative_neuron[i]==max(active[i])]
          atvie_neuron_filter=[ative_neuron[i] for i in range(int(len(active))) if ative_neuron[i]==max(active[i])]
     else:
          filter=0.255
          X=[Position[i][0] for i in range(int(len(active))) if ative_neuron[i]>filter]
          Y=[Position[i][1] for i in range(int(len(active))) if ative_neuron[i]>filter]
          atvie_neuron_filter=[ative_neuron[i] for i in range(int(len(active))) if ative_neuron[i]>filter]
     fig,ax=plt.subplots()
     # plt.scatter(X_or,Y_or,s=2)
     # plt.hist2d(X,Y,bins=80,norm=LogNorm())
     print('X',len(X))
     plt.plot(X_or,Y_or,'o',markersize=1.0,color=(0.5,0.5,0.5))
     plt.plot(X,Y,'o',markersize=5.0,color=(0.8,0,0))
     ax.set_ylabel('y')
     ax.set_xlabel('x')
     # plt.xlim(-3,-1)
     # plt.ylim(-3,1.6)
     # fig.colorbar(ax_plot,ax=ax)
     plt.show()
     fig.savefig('six.pdf',dpi=600,format='pdf')
if activ_test:
     def func(a,x):
          return a*x
     end=800
     T=np.arange(0.05,end,0.05)
     X=[Position[i][0] for i in range(int(len(active)))]
     Y=[Position[i][1] for i in range(int(len(active)))]
     derivert_Y=[angle[i+1]-angle[i] for i in range(len(angle)-1)]
     V_x=[V[i][0] for i in range(len(derivert_Y))]
     V_y=[V[i][1] for i in range(len(derivert_Y))]
     derivert_X=[angle_x[i+1]-angle_x[i] for i in range(len(angle_x)-1)]

     for i in range(len(derivert_X)):
          if derivert_X[i]<-180:
               derivert_X[i]+=360
          if derivert_X[i]>180:
               derivert_X[i]-=360
     for i in range(len(derivert_Y)):
          if derivert_Y[i]<-100:
               derivert_Y[i]+=360
          if derivert_Y[i]>100:
               derivert_Y[i]-=360
     derivert_X=[i/(360*dt) for i in derivert_X]
     derivert_Y=[i/(360*dt) for i in derivert_Y]

     Z=np.polyfit(derivert_Y,V_y,1 )
     p = np.poly1d(Z)
     print('Z',Z)
     print(p)
     popt,pcov=curve_fit(func,derivert_Y,V_y)
     derivert_Y=[i*popt[0] for i in derivert_Y]
     print('popt[0]',popt[0])
     T=list(T)

     fig,ax=plt.subplots()
     # ax2=plt.subplot(132)
     # ax3=plt.subplot(133)
     # ax2.set_title('Trajectory')
     # ax.set_title('Trajectory Error')
     # ax3.set_title('Local Trajectory')
     # ax.set_xlabel('Time(s)')
     # ax2.set_xlabel('Time(s)')
     # ax3.set_xlabel('Time(s)')
     # ax.set_ylabel('m')
     # ax2.set_ylabel('m')
     # ax3.set_ylabel('m')     
     error_y=[(V_y[i]-derivert_Y[i]) for i in range(len(V_y))]

     error_sum_y=[0]
     error_sum_y[0]=error_y[0]*dt
     for i in range(len(error_y)-1):
          error_sum_y.append(error_sum_y[i]+error_y[i+1]*dt)
     tr=[0]
     for i in range(len(V_y)-1):
          tr.append(tr[i]+dt*(V_y[i]))
     tr_es=[0]
     for i in range(len(derivert_Y)-1):
          tr_es.append(tr_es[i]+dt*(derivert_Y[i]))    

     # ax.plot(T,tr,label='Real Trajectory')
     # ax.plot(T,tr_es,LINESTYLE='--',label='Estimated Trajectory')
     ax.set_xlabel('Time(s)')
     ax.set_ylabel('m')
     # ax.plot(T,V_y,label='Input Velocity')
     # ax.plot(T,derivert_Y,LINESTYLE='--',label='Estimated Velocity',alpha=0.7)
     # plt.xlim(5000,5500)

     # ax.plot(T,error_y,label='Local velocity Error')
     # ax.plot(T,error_sum_y,LINESTYLE='--',label='Trajectory Error')
     print(error_sum_y[-1])
     # ax.plot(T,tr,label='Real Trajectory')
     # ax.plot(T,tr_es,LINESTYLE='--',label='Estimated Trajectory')
     # ax.plot(T,V_y)
     # ax.plot(T,derivert_Y,alpha=0.2,c='r')

     tr=sum([abs(i)*dt for i in V_y])
     print('total_tr',tr)
     plt.legend(loc=1)
     plt.grid()
     plt.show()
     fig.savefig('weight2.pdf',dpi=600,format='pdf')
# # ax.plot(X,error,label='error')
# # ax.plot(X,error_sum,label='accumulated error')
# # ax.legend(loc='upper left')
# # plt.title('Path drift with V=1.52m/s')
# # ax.plot(derivert,X)
# # ax.set_ylabel('Error(m)')
# # ax.set_xlabel('stimulus')
# # # ax.set_xlabel('time(s)')
# # X_li=X
# # derivert_li=derivert
# # X_li=np.array(X_li)
# # derivert_li=np.array(derivert_li)
# # Z=np.polyfit(derivert_env, derivert,1 )
# # p = np.poly1d(Z)
# # p_inv = np.poly1d(Z)
# # print(p)
# # yv=[]
# # for i in X:
# #      yv.append(fuc(i,a,b,c,d))
# # plt.plot(X,yv)
# # pylab.plot(X_li, derivert_li, 'b^-', label='original sales growth')
# # pylab.plot(derivert_env, p(derivert_env),label=f'y = {Z[0]}x + {Z[1]}')
# # pylab.plot(derivert, p_inv(derivert),label=f'y = {Z[0]}x + {Z[1]}')
# # pylab.plot(X, p(X),label=f'y = {Z[0]}x + {Z[1]}')
# # print(derivert_li[:100])
# # plt.xlim(0,0.01)
# # plt.ylim(0,40)
# plt.show()