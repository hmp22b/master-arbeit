n_hdc = 100
lam_hdc = 25824.08
weight_av_stim = 0.178124
# lam = 0.8
lam = 13
beta = 3/pow(lam, 2)
gamma = 1.05*beta
a = 1
n_x = 20
n_y = 18
dx = 0.1
dy = 0.1
w_f2gc=0.02
weight_gc_stim = 0.12
# weight_gc_stim = 0.2
gc_weight_sigma=0.13
lam_gc=298.246658903025
g2s=1
tau_gc=1.0
# gc_weight_minus=0.05
gc_weight_minus=0.02
# gc_weight_alm=0.95
gc_weight_alm=0.95



#######################parameters for model 1##########################
tau_gc_1=10.0
a=6.34
b=10.0
c=0.5
beta_gc=0.8