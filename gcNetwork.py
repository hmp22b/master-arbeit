import numpy as np
import copy
import random
from helper import angleDistAbs
from neuron import phi, phi_inv
import matplotlib.pyplot as plt
from gcTargetPeak import gctargetPeak

# connectivityFunction(i, j) = weight between i and j


def addGCAttractor(networkTopology, n_x, n_y, connectivityFunction):
    networkTopology.addLayer('gc_attractor', n_x*n_y)
    networkTopology.connectLayers(
        'gc_attractor', 'gc_attractor', connectivityFunction)
    networkTopology.vectorizeConnections('gc_attractor', 'gc_attractor')


def addGCShiftLayers(networkTopology, n, connGCL, connLGC, connGCR, connRGC, connGCU, connUGC, connGCD, connDGC):
    networkTopology.addLayer('gc_shift_left', n)
    networkTopology.addLayer('gc_shift_right', n)
    networkTopology.addLayer('gc_shift_up', n)
    networkTopology.addLayer('gc_shift_down', n)
    networkTopology.connectLayers('gc_attractor', 'gc_shift_left', connLGC)
    networkTopology.connectLayers('gc_shift_left', 'gc_attractor', connGCL)
    networkTopology.connectLayers('gc_attractor', 'gc_shift_right', connRGC)
    networkTopology.connectLayers('gc_shift_right', 'gc_attractor', connGCR)
    networkTopology.connectLayers('gc_attractor', 'gc_shift_up', connUGC)
    networkTopology.connectLayers('gc_shift_up', 'gc_attractor', connGCU)
    networkTopology.connectLayers('gc_attractor', 'gc_shift_down', connDGC)
    networkTopology.connectLayers('gc_shift_down', 'gc_attractor', connGCD)
    networkTopology.vectorizeConnections('gc_attractor', 'gc_shift_left')
    networkTopology.vectorizeConnections('gc_shift_left', 'gc_attractor')
    networkTopology.vectorizeConnections('gc_attractor', 'gc_shift_right')
    networkTopology.vectorizeConnections('gc_shift_right', 'gc_attractor')
    networkTopology.vectorizeConnections('gc_attractor', 'gc_shift_up')
    networkTopology.vectorizeConnections('gc_shift_up', 'gc_attractor')
    networkTopology.vectorizeConnections('gc_attractor', 'gc_shift_down')
    networkTopology.vectorizeConnections('gc_shift_down', 'gc_attractor')

# initializes gc by applying current corresponding to firing rates 10% of the target function


def initializeGC(networkInstance, debug=True):
    def printDebug(s):
        if debug:
            print(s)
    # compute the target firing rates at the neurons' preferred directions
    n = len(networkInstance.getLayer('gc_attractor'))
    U = [0 for i in range(n)]

    # timestep: 0.5 ms
    dt = 0.0005
    # stimulus time
    t_stim = 0.05
    # settling time
    t_settle = 0.05
    # interval time
    t_interval = 0.05
    # stopping condition: total change in t_interval less than eps
    eps = 0.001

    # apply stimulus
    networkInstance.setStimulus('gc_attractor', lambda i: U[i])

    # simulate for ts timesteps
    printDebug("GC initialization: stimulus applied")
    for i in np.arange(0.0, t_stim, dt):
        networkInstance.step(dt)
        rate=networkInstance.getLayer('gc_attractor')
        print('change for inital GC rate',rate)
    printDebug("GC initialization: stimulus removed")

    # remove stimulus
    networkInstance.setStimulus('gc_attractor', lambda i: 0)

    # simulate for t_settle
    for i in np.arange(0.0, t_settle, dt):
        networkInstance.step(dt)

    # simulate in episodes of ts timesteps until the total change in an episode is less than eps
    delta = 2*eps
    it = 0
    while delta > eps:
        it += 1
        delta = 0
        for _ in np.arange(0.0, t_interval, dt):
            ratesBefore = copy.copy(networkInstance.getLayer('gc_attractor'))
            networkInstance.step(dt)
            ratesAfter = networkInstance.getLayer('gc_attractor')
            delta += sum([abs(ratesAfter[i] - ratesBefore[i])
                          for i in range(n)])
        printDebug(
            "gc initialization: iteration {} done with total change {}".format(it, delta))
    printDebug("gc initialization: done.")
