import math
import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.fftpack import fft2, ifft2
from hdcTargetPeak import targetPeakDefault
from neuron import phi, phi_inv
from params import n_x,n_y
from gcAttractorConnectivity import GCAttractorConnectivity
attrConn = GCAttractorConnectivity(n_x, n_y)

# def connection(i, j):
#     d = math.sqrt(3.0)/2.0

#     def cartesian(i):
#         return (i % 10, int(i/10 % 9))

#     def c(x, y):
#         return (x-0.5)/10, d*(y-0.5)/9

#     def euclidean_length(x, y):
#         return math.sqrt(x**2+y**2)

#     def disttri(i, j):
#         sj = [(0.0, 0.0), (-0.5, d), (-0.5, -d), (0.5, d),
#                 (0.5, -d), (-1.0, 0.0), (1.0, 0.0)]
#         xi, yi = cartesian(i)
#         xi, yi = c(xi, yi)
#         xj, yj = cartesian(j)
#         xj, yj = c(xj, yj)
#         return min([euclidean_length(xi-xj+xs, yi-yj+ys) for xs, ys in sj])

#     return cartesian(j),69*np.exp(-disttri(i, j)**2/0.1**2)+2.72
def connection(i, j):
    d = math.sqrt(3.0)/2.0

    def cartesian(i):
        return (i % n_x, int(i/n_x % n_y))

    def c(x, y):
        return (x-0.5)/n_x, d*(y-0.5)/n_y

    def euclidean_length(x, y):
        return math.sqrt(x**2+y**2)

    def disttri(i, j):
        sj = [(0.0, 0.0), (-0.5, d), (-0.5, -d), (0.5, d),
                (0.5, -d), (-1.0, 0.0), (1.0, 0.0)]
        xi, yi = cartesian(i)
        xi, yi = c(xi, yi)
        xj, yj = cartesian(j)
        xj, yj = c(xj, yj)
        return format(min([euclidean_length(xi-xj+xs, yi-yj+ys) for xs, ys in sj]),'.12f')
    # print('weight_dist',self.weight_dist)
    # return 0.3*np.exp(-disttri(i, j)**2/0.24**2)-0.05
    return cartesian(j),attrConn.weight_dist[disttri(i, j)]

def derivation_x(to, fr):
    d = math.sqrt(3.0)/2.0

    def cartesian(i):
        return (i % n_x, int(i/n_x % n_y))

    def c(x, y):
        return (x-0.5)/n_x, d*(y-0.5)/n_y

    def euclidean_length(x, y):
        return math.sqrt(x**2+y**2)

    def disttri(i, j):
        sj = [(0.0, 0.0), (-0.5, d), (-0.5, -d), (0.5, d),
                (0.5, -d), (-1.0, 0.0), (1.0, 0.0)]
        xi, yi = cartesian(i)
        xi, yi = c(xi, yi)
        xj, yj = cartesian(j)
        xj, yj = c(xj, yj)
        return format(min([euclidean_length(xi-xj+xs, yi-yj+ys) for xs, ys in sj]),'.12f')

    def disttri_x(i, j):
        sj = [(0.0, 0.0), (-0.5, d), (-0.5, -d), (0.5, d),
                (0.5, -d), (-1.0, 0.0), (1.0, 0.0)]
        xi, yi = cartesian(i)
        if xi !=n_x:    
                xi+=1
        else:
            xi=0
        xi, yi = c(xi, yi)
        xj, yj = cartesian(j)
        xj, yj = c(xj, yj)
        return format(min([euclidean_length(xi-xj+xs, yi-yj+ys) for xs, ys in sj]),'.12f')
    der_x=attrConn.weight_dist[disttri_x(to, fr)]-attrConn.weight_dist[disttri(to, fr)]
    return cartesian(fr),der_x

l = {}
for i in range(90):
    location,w = derivation_x(55, i)
    # location,w = connection(55, i)
    l.update({tuple(location): w})
X=[]
Y=[]
V=[]
for key,value in l.items():
    X.append(key[0])
    Y.append(key[1])
    V.append(value)
ax=plt.subplot(111,projection='3d')
ax.scatter(X,Y,V)
ax.set_xlabel('X')
ax.set_ylabel('Y')
plt.show()




def weight_list(weight):
    d = math.sqrt(3.0)/2.0

    def cartesian(i):
        return (i % 10, int(i/10 % 9))

    def c(x, y):
        return (x-0.5)/10, d*(y-0.5)/9

    def euclidean_length(x, y):
        return math.sqrt(x**2+y**2)

    def disttri_xy(x, y):
        sj = [(0.0, 0.0), (-0.5, d), (-0.5, -d), (0.5, d),
                (0.5, -d), (-1.0, 0.0), (1.0, 0.0)]
        xi, yi = x,y
        xi, yi = c(xi, yi)
        xj, yj = 0,0
        xj, yj = c(xj, yj)
        return min([euclidean_length(xi-xj+xs, yi-yj+ys) for xs, ys in sj])
    
    l={}
    for x in range(n_x):
        for y in range(n_y):
            l.update({disttri_xy(x,y): np.real(weight[x,y])})
    return l

F=np.zeros([10,9])
U=np.zeros([10,9])
for x in X:
    for y in Y:
        F[x,y]=l[(x,y)]
        U[x,y]=phi_inv(l[(x,y)])


F_ = fft2(F)
U_ = fft2(U)

W_ = np.zeros([10,9])
for i in range(10):
    for j in range(9):
        W_[i,j]=(U_[i,j]*F_[i,j]) / (500 + (abs(F_[i,j]))**2)
W = ifft2(W_)

#weight
X=[]
Y=[]
# V=[]
# for x in range(10):
#     for y in range(9):
#         V.append(np.real(W[x,y]))
#         X.append(x)
#         Y.append(y)
weight_dist=weight_list(W)
print('real_weight',len(weight_dist))
for key,value in weight_dist.items():
    X.append(key)
    Y.append(value)
ax=plt.subplot(111)
ax.scatter(X,Y)
ax.set_xlabel('X')
ax.set_ylabel('Y')
plt.show()