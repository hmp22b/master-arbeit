# from pybullet_environment import PybulletEnvironment
from bullet_reset import pionnerWolrd
import time
import numpy as np
from tqdm import tqdm
import pickle
import helper
dt_robot = 0.05
env_visualize=True
env_model='maze'
t_episode = 16000
# env = PybulletEnvironment(1/dt_robot, env_visualize, env_model)
env=pionnerWolrd(1/dt_robot,True)
env.reset()
Theta=[]
V=[]
Position=[]
for t in tqdm(np.arange(0.0, t_episode, dt_robot)):
    theta, v,position = env.step()
    print(v)
    Theta.append(theta)
    V.append(v)
    Position.append(position)

with open('data_env_11_0.05_big.pkl','wb') as in_data:
    pickle.dump(Theta,in_data,pickle.HIGHEST_PROTOCOL)
    pickle.dump(V,in_data,pickle.HIGHEST_PROTOCOL)
    pickle.dump(Position,in_data,pickle.HIGHEST_PROTOCOL)




# with open('data_env.pkl','rb') as out_data:
#     Theta=pickle.load(out_data)
#     # print('Theta',Theta)
#     V=pickle.load(out_data)
#     # print('V',V)
#     Position=pickle.load(out_data)

# print(max(map(max,V)))
  
# with open('data_env_8_0.05_big.pkl','rb') as out_data:
#      Theta=pickle.load(out_data)
#      print('Theta',len(Theta))
#      V=pickle.load(out_data)
#      # print('V',V)
#      Position=pickle.load(out_data)

# V_x=[V[i][0] for i in range(len(V))]
# V_y=[V[i][1] for i in range(len(V))]
# print('min_x',min(V_x))
# print('min_y',min(V_y))
# print('max_x',max(V_x))
# print('max_y',max(V_y))