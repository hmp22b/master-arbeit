
import numpy as np
from scipy.fftpack import fft, ifft
from hdcTargetPeak import targetPeakDefault

from neuron import phi, phi_inv, tau
from helper import angleDistAbs
import matplotlib.pyplot as plt
f = targetPeakDefault
x = [((i/100)**2+(j/100)**2)**0.5 * 2*np.pi for i in range(-50, 50, 1)
     for j in range(-50, 50, 1)]
x = list(set(x))
x.sort()
x = [i for i in x if i <= np.pi]
F = [f(i)
     for i in x]
# F = [f(i * (2*np.pi / 1000)) for i in range(-500, 500, 1)]
U = [phi_inv(f) for f in F]

# compute fourier transforms
F_ = fft(F)
U_ = fft(U)

# compute fourier coefficients of W according to equation
W_ = []
for i in range(len(F_)):
    W_.append((U_[i]*F_[i]) / (28240.08 + (abs(F_[i]))**2))

    # inverse fourier to get W
W = ifft(W_)
X = range(len(W))
Y = F
print(len(W))
plt.plot(X, Y)
plt.show()
