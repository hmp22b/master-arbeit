import math
import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.fftpack import fft2, ifft2
from hdcTargetPeak import targetPeakDefault
from neuron import phi, phi_inv
from params import n_x,n_y,dx,dy,beta,gamma,gc_weight_alm,gc_weight_sigma,gc_weight_minus,w_f2gc
from matplotlib import cm

def connection(i, j):
    d = math.sqrt(3.0)/2.0

    def cartesian(i):
        return (i % n_x, int(i/n_x % n_y))

    def c(x, y):
        return (x-0.5)/n_x, d*(y-0.5)/n_y

    def euclidean_length(x, y):
        return math.sqrt(x**2+y**2)

    def disttri(i, j):
        sj = [(0.0, 0.0), (-0.5, d), (-0.5, -d), (0.5, d),
                (0.5, -d), (-1.0, 0.0), (1.0, 0.0)]
        xi, yi = cartesian(i)
        xi, yi = c(xi, yi)
        xj, yj = cartesian(j)
        xj, yj = c(xj, yj)
        # return format(min([euclidean_length(xi-xj+xs, yi-yj+ys) for xs, ys in sj]),'.12f')
        return min([euclidean_length(xi-xj+xs, yi-yj+ys) for xs, ys in sj])
    # print('weight_dist',self.weight_dist)
    return cartesian(j),gc_weight_alm*np.exp(-disttri(i, j)**2/gc_weight_sigma**2)-gc_weight_minus

def derivation_y(to, fr):
    d = math.sqrt(3.0)/2.0

    def cartesian(i):
        return (i % n_x, int(i/n_x % n_y))

    def c(x, y):
        return (x-0.5)/n_x, d*(y-0.5)/n_y

    def euclidean_length(x, y):
        return math.sqrt(x**2+y**2)

    def disttri(i, j):
        sj = [(0.0, 0.0), (-0.5, d), (-0.5, -d), (0.5, d),
                (0.5, -d), (-1.0, 0.0), (1.0, 0.0)]
        xi, yi = cartesian(i)
        xi, yi = c(xi, yi)
        xj, yj = cartesian(j)
        xj, yj = c(xj, yj)
        return min([euclidean_length(xi-xj+xs, yi-yj+ys) for xs, ys in sj])

    def disttri_y(i, j):
        sj = [(0.0, 0.0), (-0.5, d), (-0.5, -d), (0.5, d),
                (0.5, -d), (-1.0, 0.0), (1.0, 0.0)]
        xi, yi = cartesian(i)
        xi, yi = c(xi, yi)
        xj, yj = cartesian(j)
        xj, yj = c(xj, yj)
        return min([euclidean_length(xi-xj+xs, yi-yj+ys+dy) for xs, ys in sj])
    der_y = ((gc_weight_alm*np.exp(-disttri_y(to, fr)**2/gc_weight_sigma**2)-gc_weight_minus) -
                (gc_weight_alm*np.exp(-disttri(to, fr)**2/gc_weight_sigma**2)-gc_weight_minus))/dy
    return cartesian(fr),der_y


def derivation_minusy(to, fr):
    d = math.sqrt(3.0)/2.0

    def cartesian(i):
        return (i % n_x, int(i/n_x % n_y))

    def c(x, y):
        return (x-0.5)/n_x, d*(y-0.5)/n_y

    def euclidean_length(x, y):
        return math.sqrt(x**2+y**2)

    def disttri(i, j):
        sj = [(0.0, 0.0), (-0.5, d), (-0.5, -d), (0.5, d),
                (0.5, -d), (-1.0, 0.0), (1.0, 0.0)]
        xi, yi = cartesian(i)
        xi, yi = c(xi, yi)
        xj, yj = cartesian(j)
        xj, yj = c(xj, yj)
        return min([euclidean_length(xi-xj+xs, yi-yj+ys) for xs, ys in sj])

    def disttri_y(i, j):
        sj = [(0.0, 0.0), (-0.5, d), (-0.5, -d), (0.5, d),
                (0.5, -d), (-1.0, 0.0), (1.0, 0.0)]
        xi, yi = cartesian(i)
        xi, yi = c(xi, yi)
        xj, yj = cartesian(j)
        xj, yj = c(xj, yj)
        return min([euclidean_length(xi-xj+xs, yi-yj+ys-dy) for xs, ys in sj])
    der_y = ((gc_weight_alm*np.exp(-disttri_y(to, fr)**2/gc_weight_sigma**2)-gc_weight_minus) -
                (gc_weight_alm*np.exp(-disttri(to, fr)**2/gc_weight_sigma**2)-gc_weight_minus))/dy
    return cartesian(fr),der_y

def derivation_x(to, fr):

    d = math.sqrt(3.0)/2.0
    def cartesian(i):
        return (i % n_x, int(i/n_x % n_y))

    def c(x, y):
        return (x-0.5)/n_x, d*(y-0.5)/n_y

    def euclidean_length(x, y):
        return math.sqrt(x**2+y**2)

    def disttri(i, j):
        sj = [(0.0, 0.0), (-0.5, d), (-0.5, -d), (0.5, d),
                (0.5, -d), (-1.0, 0.0), (1.0, 0.0)]
        xi, yi = cartesian(i)
        xi, yi = c(xi, yi)
        xj, yj = cartesian(j)
        xj, yj = c(xj, yj)
        return min([euclidean_length(xi-xj+xs, yi-yj+ys) for xs, ys in sj])

    def disttri_x(i, j):
        sj = [(0.0, 0.0), (-0.5, d), (-0.5, -d), (0.5, d),
                (0.5, -d), (-1.0, 0.0), (1.0, 0.0)]
        xi, yi = cartesian(i)
        xi, yi = c(xi, yi)
        xj, yj = cartesian(j)
        xj, yj = c(xj, yj)
        return min([euclidean_length(xi-xj+xs+dx, yi-yj+ys) for xs, ys in sj])
    der_x =((gc_weight_alm*np.exp(-disttri_x(to, fr)**2/gc_weight_sigma**2)-gc_weight_minus) -
                (gc_weight_alm*np.exp(-disttri(to, fr)**2/gc_weight_sigma**2)-gc_weight_minus))/dx
    return cartesian(fr),der_x


def derivation_minusx(to, fr):
    d = math.sqrt(3.0)/2.0

    def cartesian(i):
        return (i % n_x, int(i/n_x % n_y))

    def c(x, y):
        return (x-0.5)/n_x, d*(y-0.5)/n_y

    def euclidean_length(x, y):
        return math.sqrt(x**2+y**2)

    def disttri(i, j):
        sj = [(0.0, 0.0), (-0.5, d), (-0.5, -d), (0.5, d),
                (0.5, -d), (-1.0, 0.0), (1.0, 0.0)]
        xi, yi = cartesian(i)
        xi, yi = c(xi, yi)
        xj, yj = cartesian(j)
        xj, yj = c(xj, yj)
        # return format(min([euclidean_length(xi-xj+xs, yi-yj+ys) for xs, ys in sj]),'.12f')
        return min([euclidean_length(xi-xj+xs, yi-yj+ys) for xs, ys in sj])

    def disttri_x(i, j):
        sj = [(0.0, 0.0), (-0.5, d), (-0.5, -d), (0.5, d),
                (0.5, -d), (-1.0, 0.0), (1.0, 0.0)]
        xi, yi = cartesian(i)
        # if xi !=n_x:    
        #      xi+=1
        # else:
        #     xi=0
        xi, yi = c(xi, yi)
        xj, yj = cartesian(j)
        xj, yj = c(xj, yj)
        # return format(min([euclidean_length(xi-xj+xs+dx, yi-yj+ys) for xs, ys in sj]),'.12f')
        return min([euclidean_length(xi-xj+xs-dx, yi-yj+ys) for xs, ys in sj])
    der_x =((gc_weight_alm*np.exp(-disttri_x(to, fr)**2/gc_weight_sigma**2)-gc_weight_minus) -
                (gc_weight_alm*np.exp(-disttri(to, fr)**2/gc_weight_sigma**2)-gc_weight_minus))/dx
    # der_x=attrConn.weight_dist[disttri_x(to, fr)]-attrConn.weight_dist[disttri(to, fr)]
    return cartesian(fr),der_x
def hat(d):
    return gc_weight_alm*np.exp(-d**2/gc_weight_sigma**2)-gc_weight_minus

l1 = {}
l2={}
for i in range(n_x*n_y):
    location1,w1 = connection(170, i)
    l1.update({tuple(location1):w1})
    location2,w2 = connection(0, i)
    l2.update({tuple(location2):w2})
X1=[]
Y1=[]
V1=[]
X2=[]
Y2=[]
V2=[]
Z1 = np.zeros((n_x, n_y))
Z2 = np.zeros((n_x, n_y))
# print(l)
for key,value in l1.items():
    X1.append(key[0])
    Y1.append(key[1])
    V1.append(value)
    Z1[key[0],key[1]]=w_f2gc*value


for key,value in l2.items():
    X2.append(key[0])
    Y2.append(key[1])
    V2.append(value)
    Z2[key[0],key[1]]=w_f2gc*value

# print(max(V1))
# print(max(V2))
# ax=plt.subplot(121,projection='3d')
# ax.set_title('Weight from ${Neuron_{x,y}}$ to ${Neuron_{10,9}}$')
# ax.scatter(X1,Y1,V1,c=V1)
# ax.set_xlabel('x')
# ax.set_ylabel('y')

# ax2=plt.subplot(122,projection='3d')
# ax2.set_title('Weight from ${Neuron_{x,y}}$ to ${Neuron_{10,9}}$')
# ax2.scatter(X2,Y2,V2,c=V2)
# ax2.set_xlabel('x')
# ax2.set_ylabel('y')
# plt.show()
X1 = np.arange(n_x)
Y1 = np.arange(n_y)
# for i in range(len(X1)):
#     for j in range(len(Y1)):
#         Z1[i,j]=Z1[i,j]
activition=[0.016044214, 0.00026849844, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.00026850868, 0.01604423, 0.034846365, 0.054101467, 0.07075524, 0.08202779, 0.086010695, 0.08202779, 0.07075524, 0.054101467, 0.034846365, 0.035336077, 0.0129821, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.012982115, 0.035336077, 0.06109476, 0.08680189, 0.10863733, 0.12325215, 0.12838864, 0.12325215, 0.10863733, 0.08680189, 0.06109476, 0.055843353, 0.026966602, 0.004662387, 0.0, 0.0, 0.0, 0.0, 0.0, 0.004662402, 0.026966631, 0.055843413, 0.08827257, 0.120022774, 0.14664245, 0.16431975, 0.17050982, 0.16431975, 0.14664245, 0.120022774, 0.08827257, 0.0746789, 0.040121794, 0.012801826, 0.0, 0.0, 0.0, 0.0, 0.0, 0.012801856, 0.040121853, 0.07467902, 0.11277771, 0.14958644, 0.18017912, 0.20039034, 0.2074511, 0.20039034, 0.18017912, 0.14958644, 0.11277759, 0.08904016, 0.050311565, 0.019226253, 0.0, 0.0, 0.0, 0.0, 0.0, 0.019226283, 0.050311625, 0.08904016, 0.13123846, 0.17167354, 0.20510292, 0.22712135, 0.23480248, 0.22712135, 0.20510292, 0.17167354, 0.13123822, 0.09675467, 0.055833817, 0.022745043, 0.0, 0.0, 0.0, 0.0, 0.0, 0.022745073, 0.055833876, 0.09675467, 0.14108968, 0.18340707, 0.21830654, 0.24126077, 0.24926329, 0.24126077, 0.21830654, 0.18340707, 0.14108968, 0.096653104, 0.055760562, 0.022697985, 0.0, 0.0, 0.0, 0.0, 0.0, 0.022698015, 0.055760622, 0.09665322, 0.14096093, 0.18325448, 0.21813583, 0.24107838, 0.24907684, 0.24107838, 0.21813583, 0.18325448, 0.14096093, 0.0887506, 0.050103784, 0.019093722, 0.0, 0.0, 0.0, 0.0, 0.0, 0.019093752, 0.050103843, 0.0887506, 0.13086987, 0.17123604, 0.20461202, 0.2265966, 0.23426628, 0.2265966, 0.20461202, 0.17123604, 0.13086987, 0.0742445, 0.039813578, 0.012607917, 0.0, 0.0, 0.0, 0.0, 0.0, 0.012607932, 0.039813578, 0.0742445, 0.11222005, 0.14892101, 0.17942977, 0.1995883, 0.2066307, 0.1995883, 0.17942977, 0.14892101, 0.11222005, 0.055327892, 0.026607305, 0.0044412985, 0.0, 0.0, 0.0, 0.0, 0.0, 0.004441306, 0.026607335, 0.055327892, 0.08760214, 0.11921549, 0.14572859, 0.16333842, 0.16950536, 0.16333842, 0.14572859, 0.11921549, 0.08760214, 0.034813225, 0.012626663, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.012626663, 0.034813225, 0.060401857, 0.085956335, 0.10767186, 0.12221074, 0.12732112, 0.12221074, 0.10767186, 0.085956335, 0.060401857, 0.015583128, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.015583143, 0.034219086, 0.05332154, 0.06985402, 0.08104932, 0.08500576, 0.08104932, 0.06985402, 0.05332154, 0.034219086, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0121459365, 0.0250673, 0.036529362, 0.044412434, 0.047219038, 0.044412434, 0.036529362, 0.0250673, 0.0121459365, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0037081353, 0.010718957, 0.015651435, 0.017426312, 0.015651435, 0.010718957, 0.0037081316, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.00414557, 0.011245415, 0.016234726, 0.018028975, 0.016234726, 0.0112454, 0.00414557, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.00032129418, 0.01263988, 0.025697947, 0.03727007, 0.045223534, 0.04805422, 0.045223534, 0.03727007, 0.025697947, 0.012639865, 0.0003212858, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
Z=np.zeros((n_x,n_y))
for i in range(len(activition)):
    Z[i % n_x,int(i/n_x % n_y)]=activition[i]
fig = plt.figure()
ax = fig.gca(projection='3d')

XX, YY = np.meshgrid(X1, Y1)
surf = ax.plot_surface(XX, YY,  np.transpose(Z), cmap=cm.coolwarm,
                       linewidth=0, antialiased=True)
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')
# ax.set_zlabel('Firing Rate')
# ax.set_title('Weight from ${Neuron_{x,y}}$ to ${Neuron_{10,9}}$')
# ax.set_title('Weight from ${Neuron_{x,y}}$ to ${Neuron_{0,0}}$')                       
# position=fig.add_axes([0.15, 0.05, 0.7, 0.03])
fig.colorbar(surf, shrink=0.5, aspect=5)
plt.show()
fig.savefig('weight2.pdf',dpi=600,format='pdf')




# ax=plt.subplot(111)
# X=np.arange(0,1.5,0.0005)
# Y=[hat(d) for d in X]
# ax.set_xlabel('Distance in Intervals between Reurons')
# ax.set_ylabel('Sysnaptic Weight')
# plt.tick_params(labelsize=10)
# plt.axhline(y=0,color='r',linestyle='--')
# ax.plot(X,Y)
# plt.show()

