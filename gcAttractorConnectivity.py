import numpy as np
from scipy.fftpack import fft2, ifft2
from gcTargetPeak import gctargetPeakDefault
import math
from neuron import phi, phi_inv, tau
from helper import angleDistAbs
from params import a, gamma, beta,lam_gc,gc_weight_sigma,gc_weight_alm,gc_weight_minus


class GCAttractorConnectivity:
    # n: number of neurons
    # lam: lambda from Zhang 1995 paper
    # f: target activity peak function, None for default
    def __init__(self, n_x, n_y, f=None):
        # initialize and compute F
        self.n_x = n_x
        self.n_y = n_y
        # compute W and store
        # if f == None:
        #     f = targetPeakDefault
        # self.f = f
        # F = [f(((i/n_x)**2+(j/n_y)**2)**0.5*2*np.pi)
        #      for i in range(n_x) for j in range(n_y)]
        W = self.connection
        self.conns = W
    #     f=gctargetPeakDefault
    #     self.f=f
    #     l = {}
    #     for i in range(self.n_x*self.n_y):
    #         location,w = self.f(i)
    #         l.update({tuple(location): w})
    #     X=[]
    #     Y=[]
    #     V=[]
    #     for key,value in l.items():
    #         X.append(key[0])
    #         Y.append(key[1])
    #         V.append(value)       
    #     F=np.zeros([10,9])
    #     U=np.zeros([10,9])
    #     for x in X:
    #         for y in Y:
    #             F[x,y]=l[(x,y)]
    #             U[x,y]=phi_inv(l[(x,y)])
    #     F_ = fft2(F)
    #     U_ = fft2(U)

    #     W_ = np.zeros([10,9])
    #     for i in range(10):
    #         for j in range(9):
    #             W_[i,j]=(U_[i,j]*F_[i,j]) / (lam_gc + (abs(F_[i,j]))**2)
    #     W = ifft2(W_)
    #     self.weight_dist=self.weight_list(W)
        
    # def weight_list(self,weight):
    #     d = math.sqrt(3.0)/2.0
    #     def c(x, y):
    #         return (x-0.5)/10, d*(y-0.5)/9

    #     def euclidean_length(x, y):
    #         return math.sqrt(x**2+y**2)

    #     def disttri_xy(x, y):
    #         sj = [(0.0, 0.0), (-0.5, d), (-0.5, -d), (0.5, d),
    #                 (0.5, -d), (-1.0, 0.0), (1.0, 0.0)]
    #         xi, yi = x,y
    #         xi, yi = c(xi, yi)
    #         xj, yj = 0,0
    #         xj, yj = c(xj, yj)
    #         return format(min([euclidean_length(xi-xj+xs, yi-yj+ys) for xs, ys in sj]),'.12f')
        
    #     l={}
    #     for x in range(self.n_x):
    #         for y in range(self.n_y):
    #             l.update({disttri_xy(x,y): np.real(weight[x,y])})
    #     return l
    def connection(self, i, j):
        d = math.sqrt(3.0)/2.0

        def cartesian(i):
            return (i % self.n_x, int(i/self.n_x % self.n_y))

        def c(x, y):
            return (x-0.5)/self.n_x, d*(y-0.5)/self.n_y

        def euclidean_length(x, y):
            return math.sqrt(x**2+y**2)

        def disttri(i, j):
            sj = [(0.0, 0.0), (-0.5, d), (-0.5, -d), (0.5, d),
                  (0.5, -d), (-1.0, 0.0), (1.0, 0.0)]
            xi, yi = cartesian(i)
            xi, yi = c(xi, yi)
            xj, yj = cartesian(j)
            xj, yj = c(xj, yj)
            # return format(min([euclidean_length(xi-xj+xs, yi-yj+ys) for xs, ys in sj]),'.12f')
            return min([euclidean_length(xi-xj+xs, yi-yj+ys) for xs, ys in sj])
        # print('weight_dist',self.weight_dist)
        return gc_weight_alm*np.exp(-disttri(i, j)**2/gc_weight_sigma**2)-gc_weight_minus
        # return self.weight_dist[disttri(i, j)]