
import numpy as np
from scipy.fftpack import fft, ifft
from hdcTargetPeak import targetPeakDefault
from neuron import phi, phi_inv
F=np.zeros([10,9])
U = [phi_inv(f) for f in F]

F_ = fft(F)
U_ = fft(U)

W_ = []
for i in range(len(F_)):
     W_.append((U_[i]*F_[i]) / (25824.08 + (abs(F_[i]))**2))
W = ifft(W_)