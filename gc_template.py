from gcnetwork import NetworkTopology
import gcNetwork
import numpy as np
from gcAttractorConnectivity import GCAttractorConnectivity
from params import n_x, n_y, beta, gamma, a, dx, dy,w_f2gc,gc_weight_sigma,gc_weight_alm,gc_weight_minus,g2s
import math


def generateGC():
    attrConn = GCAttractorConnectivity(n_x, n_y)

    # initialize gc
    topo = NetworkTopology()
    gcNetwork.addGCAttractor(topo, n_x, n_y, attrConn.connection)

    # add shift layers
    # gc -> shift layers
    def connGCS(i, j): return attrConn.connection(i, j) * g2s
    
    # shift layers -> gc

    def derivation_x(to, fr):
        d = math.sqrt(3.0)/2.0

        def cartesian(i):
            return (i % n_x, int(i/n_x % n_y))

        def c(x, y):
            return (x-0.5)/n_x, d*(y-0.5)/n_y

        def euclidean_length(x, y):
            return math.sqrt(x**2+y**2)

        def disttri(i, j):
            sj = [(0.0, 0.0), (-0.5, d), (-0.5, -d), (0.5, d),
                  (0.5, -d), (-1.0, 0.0), (1.0, 0.0)]
            xi, yi = cartesian(i)
            xi, yi = c(xi, yi)
            xj, yj = cartesian(j)
            xj, yj = c(xj, yj)
            # return format(min([euclidean_length(xi-xj+xs, yi-yj+ys) for xs, ys in sj]),'.12f')
            return min([euclidean_length(xi-xj+xs, yi-yj+ys) for xs, ys in sj])

        def disttri_x(i, j):
            sj = [(0.0, 0.0), (-0.5, d), (-0.5, -d), (0.5, d),
                  (0.5, -d), (-1.0, 0.0), (1.0, 0.0)]
            xi, yi = cartesian(i)
            # if xi !=n_x:    
            #      xi+=1
            # else:
            #     xi=0
            xi, yi = c(xi, yi)
            xj, yj = cartesian(j)
            xj, yj = c(xj, yj)
            # return format(min([euclidean_length(xi-xj+xs+dx, yi-yj+ys) for xs, ys in sj]),'.12f')
            return min([euclidean_length(xi-xj+xs+dx, yi-yj+ys) for xs, ys in sj])
        der_x = w_f2gc*((gc_weight_alm*np.exp(-disttri_x(to, fr)**2/gc_weight_sigma**2)-gc_weight_minus) -
                 (gc_weight_alm*np.exp(-disttri(to, fr)**2/gc_weight_sigma**2)-gc_weight_minus))/dx
        # der_x=attrConn.weight_dist[disttri_x(to, fr)]-attrConn.weight_dist[disttri(to, fr)]
        return der_x

    def derivation_mx(to, fr):
        d = math.sqrt(3.0)/2.0

        def cartesian(i):
            return (i % n_x, int(i/n_x % n_y))

        def c(x, y):
            return (x-0.5)/n_x, d*(y-0.5)/n_y

        def euclidean_length(x, y):
            return math.sqrt(x**2+y**2)

        def disttri(i, j):
            sj = [(0.0, 0.0), (-0.5, d), (-0.5, -d), (0.5, d),
                  (0.5, -d), (-1.0, 0.0), (1.0, 0.0)]
            xi, yi = cartesian(i)
            xi, yi = c(xi, yi)
            xj, yj = cartesian(j)
            xj, yj = c(xj, yj)
            # return format(min([euclidean_length(xi-xj+xs, yi-yj+ys) for xs, ys in sj]),'.12f')
            return min([euclidean_length(xi-xj+xs, yi-yj+ys) for xs, ys in sj])

        def disttri_x(i, j):
            sj = [(0.0, 0.0), (-0.5, d), (-0.5, -d), (0.5, d),
                  (0.5, -d), (-1.0, 0.0), (1.0, 0.0)]
            xi, yi = cartesian(i)
            # if xi !=n_x:    
            #      xi+=1
            # else:
            #     xi=0
            xi, yi = c(xi, yi)
            xj, yj = cartesian(j)
            xj, yj = c(xj, yj)
            # return format(min([euclidean_length(xi-xj+xs+dx, yi-yj+ys) for xs, ys in sj]),'.12f')
            return min([euclidean_length(xi-xj+xs-dx, yi-yj+ys) for xs, ys in sj])
        der_x = w_f2gc*((gc_weight_alm*np.exp(-disttri_x(to, fr)**2/gc_weight_sigma**2)-gc_weight_minus) -
                 (gc_weight_alm*np.exp(-disttri(to, fr)**2/gc_weight_sigma**2)-gc_weight_minus))/dx
        # der_x=attrConn.weight_dist[disttri_x(to, fr)]-attrConn.weight_dist[disttri(to, fr)]
        return der_x    
    
    
    
    
    def derivation_y(to, fr):
        d = math.sqrt(3.0)/2.0

        def cartesian(i):
            return (i % n_x, int(i/n_x % n_y))

        def c(x, y):
            return (x-0.5)/n_x, d*(y-0.5)/n_y

        def euclidean_length(x, y):
            return math.sqrt(x**2+y**2)

        def disttri(i, j):
            sj = [(0.0, 0.0), (-0.5, d), (-0.5, -d), (0.5, d),
                  (0.5, -d), (-1.0, 0.0), (1.0, 0.0)]
            xi, yi = cartesian(i)
            xi, yi = c(xi, yi)
            xj, yj = cartesian(j)
            xj, yj = c(xj, yj)
            # return format(min([euclidean_length(xi-xj+xs, yi-yj+ys) for xs, ys in sj]),'.12f')
            return min([euclidean_length(xi-xj+xs, yi-yj+ys) for xs, ys in sj])

        def disttri_y(i, j):
            sj = [(0.0, 0.0), (-0.5, d), (-0.5, -d), (0.5, d),
                  (0.5, -d), (-1.0, 0.0), (1.0, 0.0)]
            xi, yi = cartesian(i)
            xi, yi = c(xi, yi)
            xj, yj = cartesian(j)
            xj, yj = c(xj, yj)
            # return format(min([euclidean_length(xi-xj+xs, yi-yj+ys) for xs, ys in sj]),'.12f')
            return min([euclidean_length(xi-xj+xs, yi-yj+ys+dy) for xs, ys in sj])
        der_y = w_f2gc*((gc_weight_alm*np.exp(-disttri_y(to, fr)**2/gc_weight_sigma**2)-gc_weight_minus) -
                 (gc_weight_alm*np.exp(-disttri(to, fr)**2/gc_weight_sigma**2)-gc_weight_minus))/dy
        # der_y=attrConn.weight_dist[disttri_y(to, fr)]-attrConn.weight_dist[disttri(to, fr)]
        return der_y

    def derivation_my(to, fr):
        d = math.sqrt(3.0)/2.0

        def cartesian(i):
            return (i % n_x, int(i/n_x % n_y))

        def c(x, y):
            return (x-0.5)/n_x, d*(y-0.5)/n_y

        def euclidean_length(x, y):
            return math.sqrt(x**2+y**2)

        def disttri(i, j):
            sj = [(0.0, 0.0), (-0.5, d), (-0.5, -d), (0.5, d),
                  (0.5, -d), (-1.0, 0.0), (1.0, 0.0)]
            xi, yi = cartesian(i)
            xi, yi = c(xi, yi)
            xj, yj = cartesian(j)
            xj, yj = c(xj, yj)
            # return format(min([euclidean_length(xi-xj+xs, yi-yj+ys) for xs, ys in sj]),'.12f')
            return min([euclidean_length(xi-xj+xs, yi-yj+ys) for xs, ys in sj])

        def disttri_y(i, j):
            sj = [(0.0, 0.0), (-0.5, d), (-0.5, -d), (0.5, d),
                  (0.5, -d), (-1.0, 0.0), (1.0, 0.0)]
            xi, yi = cartesian(i)
            xi, yi = c(xi, yi)
            xj, yj = cartesian(j)
            xj, yj = c(xj, yj)
            # return format(min([euclidean_length(xi-xj+xs, yi-yj+ys) for xs, ys in sj]),'.12f')
            return min([euclidean_length(xi-xj+xs, yi-yj+ys-dy) for xs, ys in sj])
        der_y = w_f2gc*((gc_weight_alm*np.exp(-disttri_y(to, fr)**2/gc_weight_sigma**2)-gc_weight_minus) -
                 (gc_weight_alm*np.exp(-disttri(to, fr)**2/gc_weight_sigma**2)-gc_weight_minus))/dy
        # der_y=attrConn.weight_dist[disttri_y(to, fr)]-attrConn.weight_dist[disttri(to, fr)]
        return der_y


    def peak_right(to, fr):
        return derivation_x(to, fr)


    def peak_left(to, fr):
        return derivation_mx(to, fr)

    def peak_down(to, fr):
        return derivation_my(to, fr)

    def peak_up(to, fr):
        return derivation_y(to, fr)

    gcNetwork.addGCShiftLayers(topo, n_x*n_y, connGCS, peak_left,
                               connGCS, peak_right, connGCS, peak_up, connGCS, peak_down)

    # make instance
    gc = topo.makeInstance()

    # initialize
    gcNetwork.initializeGC(gc)
    return gc
