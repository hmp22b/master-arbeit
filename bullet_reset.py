import os
import pybullet as p
import pybullet_data
import time
import math
import gym
import numpy as np
import random

class pionnerWolrd(gym.Env):
    def __init__(self,rate,visualize):
        self.rate_ = rate
        self.visualize = visualize
        if self.visualize:
            p.connect(p.GUI)
            p.configureDebugVisualizer(p.COV_ENABLE_GUI, 0)        
        else:
            p.connect(p.DIRECT)
        p.setTimeStep(1.0/self.rate_)
        p.resetDebugVisualizerCamera(
            cameraDistance=5, cameraYaw=0, cameraPitch=-89, cameraTargetPosition=[0, 0, 0])
        self.time_elapsed = 0
        self.counter=0
        pass
    # pose is the robot pose-----position is the point position in world
    def transform(self, pose, position):
        euler = p.getEulerFromQuaternion(pose[1])
        R = np.array([[math.cos(euler[2]), math.sin(euler[2]), 0],
                      [-math.sin(euler[2]), math.cos(euler[2]), 0],
                      [0, 0, 1]])
        position_inrobot = R.dot(np.array(position)-np.array(pose[0]))
        return position_inrobot

    def reset(self):
        p.resetSimulation()
        p.configureDebugVisualizer(p.COV_ENABLE_RENDERING, 0)
        urdfRootPath = pybullet_data.getDataPath()
        self.lengh = 5
        self.lengh_robot = 0.2
        p.setGravity(0, 0, -9.8)
        p.setTimestep = 1./self.rate_
        self.pionnerUid = p.loadURDF("p3dx/urdf/pioneer3dx.urdf",
                                     basePosition=[0, 0, 0])

        planeUid = p.loadURDF(os.path.join(
            urdfRootPath, "plane.urdf"), basePosition=[0, 0, -0.1])
        # create the links
        box_center = [1.5, 0.1, 0.2]
        box1_id = p.createCollisionShape(p.GEOM_BOX, halfExtents=box_center)
        mass = 10000
        visualShapeId = -1
        visualShapeId = p.createVisualShape(
            shapeType=p.GEOM_BOX, rgbaColor=[0.5, 0.5, 0.5, 1], halfExtents=box_center)
        # basePosition_box = [0, 0, 0]
        # baseOrientation_box = [0, 0, 0, 1]
        # boxUid = p.createMultiBody(
        #     mass, box1_id, visualShapeId, basePosition_box)

        # box_center_2 = [0.1, 0.7, 0.2]
        # basePosition_box2 = [0, 0.8, 0]
        # box2_id = p.createCollisionShape(p.GEOM_BOX, halfExtents=box_center_2)
        # visualShapeId = p.createVisualShape(
        #     shapeType=p.GEOM_BOX, rgbaColor=[0.5, 0.5, 0.5, 1], halfExtents=box_center_2)
        # box2Uid = p.createMultiBody(
        #     mass, box2_id, visualShapeId, basePosition_box2)

        # basePosition_box3 = [0, -0.8, 0]
        # box3_id = p.createCollisionShape(p.GEOM_BOX, halfExtents=box_center_2)
        # visualShapeId = p.createVisualShape(
        #     shapeType=p.GEOM_BOX, rgbaColor=[0.5, 0.5, 0.5, 1], halfExtents=box_center_2)
        # box3Uid = p.createMultiBody(
        #     mass, box3_id, visualShapeId, basePosition_box3)

        box_center_3 = [0.1, 3.5, 0.2]
        basePosition_box4 = [3.1, 0, 0]
        visualShapeId = p.createVisualShape(
            shapeType=p.GEOM_BOX, rgbaColor=[0.5, 0.5, 0.5, 1], halfExtents=box_center_3)
        box4_id = p.createCollisionShape(p.GEOM_BOX, halfExtents=box_center_3)
        box4Uid = p.createMultiBody(
            mass, box4_id, visualShapeId, basePosition_box4)
        basePosition_box5 = [-3.1, 0, 0]
        box5_id = p.createCollisionShape(p.GEOM_BOX, halfExtents=box_center_3)
        visualShapeId = p.createVisualShape(
            shapeType=p.GEOM_BOX, rgbaColor=[0.5, 0.5, 0.5, 1], halfExtents=box_center_3)
        box5Uid = p.createMultiBody(
            mass, box5_id, visualShapeId, basePosition_box5)

        box_center_4 = [3, 0.1, 0.2]
        basePosition_box6 = [0, 3.4, 0]
        box6_id = p.createCollisionShape(p.GEOM_BOX, halfExtents=box_center_4)
        print(box6_id)
        visualShapeId = p.createVisualShape(
            shapeType=p.GEOM_BOX, rgbaColor=[0.5, 0.5, 0.5, 1], halfExtents=box_center_4)
        box6Uid = p.createMultiBody(
            mass, box6_id, visualShapeId, basePosition_box6)

        basePosition_box7 = [0, -3.4, 0]
        box7_id = p.createCollisionShape(p.GEOM_BOX, halfExtents=box_center_4)
        visualShapeId = p.createVisualShape(
            shapeType=p.GEOM_BOX, rgbaColor=[0.5, 0.5, 0.5, 1], halfExtents=box_center_4)
        box7Uid = p.createMultiBody(
            mass, box7_id, visualShapeId, basePosition_box7)

        # box_center_5 = [0.5, 0.5, 0.1]
        # basePosition_box8 = [2.5, 2.8, 0]
        # box8_id = p.createCollisionShape(p.GEOM_BOX, halfExtents=box_center_5)
        # visualShapeId = p.createVisualShape(
        #     shapeType=p.GEOM_BOX, rgbaColor=[0.5, 0.5, 0.5, 1], halfExtents=box_center_5)
        # box8Uid = p.createMultiBody(
        #     mass, box8_id, visualShapeId, basePosition_box8)

        # basePosition_box9 = [-2.5, 2.8, 0]
        # box9_id = p.createCollisionShape(p.GEOM_BOX, halfExtents=box_center_5)
        # visualShapeId = p.createVisualShape(
        #     shapeType=p.GEOM_BOX, rgbaColor=[0.5, 0.5, 0.5, 1], halfExtents=box_center_5)
        # box9Uid = p.createMultiBody(
        #     mass, box9_id, visualShapeId, basePosition_box9)

        # basePosition_box10 = [2.5, -2.8, 0]
        # box10_id = p.createCollisionShape(p.GEOM_BOX, halfExtents=box_center_5)
        # visualShapeId = p.createVisualShape(
        #     shapeType=p.GEOM_BOX, rgbaColor=[0.5, 0.5, 0.5, 1], halfExtents=box_center_5)
        # box10Uid = p.createMultiBody(
        #     mass, box10_id, visualShapeId, basePosition_box10)

        # basePosition_box11 = [-2.5, -2.8, 0]
        # box11_id = p.createCollisionShape(p.GEOM_BOX, halfExtents=box_center_5)
        # visualShapeId = p.createVisualShape(
        #     shapeType=p.GEOM_BOX, rgbaColor=[0.5, 0.5, 0.5, 1], halfExtents=box_center_5)
        # box11Uid = p.createMultiBody(
        #     mass, box11_id, visualShapeId, basePosition_box11)
        p.configureDebugVisualizer(p.COV_ENABLE_RENDERING, 1)
        # p.configureDebugVisualizer(p.COV_ENABLE_GUI, 1)
    def render(self, mode='human'):
        pass
    def close(self):
        p.disconnect()
    def braitenberg(self, rayDist,counter):
        detect = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        braitenbergL = np.array(
            [-0.8, -0.6, -0.4, -0.2, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -1.6, -1.4, -1.2, -1.0])*16
        braitenbergR = np.array(
            [-1.0, -1.2, -1.4, -1.6, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.2, -0.4, -0.6, -0.8])
        velocity_0 = 5.0 # default 2.0
        noDetectionDist = 0.6
        maxDetectionDist = 0.45
        # print("Ray Dist: ", rayDist)
        for i in range(len(rayDist)):
            if 0 < rayDist[i] < noDetectionDist:
                # something is detected
                if rayDist[i] < maxDetectionDist:
                    rayDist[i] = maxDetectionDist
                # dangerous level, the higher, the closer
                detect[i] = 1.0 - 1.0 * ((rayDist[i] - maxDetectionDist)
                                         * 1.0 / (noDetectionDist - maxDetectionDist))
            else:
                # nothing is detected
                detect[i] = 0
        if counter<=1000:
            vLeft = velocity_0
            vRight = velocity_0
        else:
            vLeft = velocity_0
            vRight = velocity_0+random.random()*5
        # print(detect)         
        for i in range(len(rayDist)):
            vLeft = vLeft + braitenbergL[i] * detect[i] * 1
            vRight = vRight + braitenbergR[i] * detect[i] * 1
        return vLeft, vRight

    def step(self):
        p.configureDebugVisualizer(p.COV_ENABLE_SINGLE_STEP_RENDERING, 1, p.COV_ENABLE_GUI, 0)
        self.counter+=1
        self.time_elapsed += 1./self.rate_
        pose = p.getBasePositionAndOrientation(self.pionnerUid)
        target = []
        origin = []
        theta_pre = p.getEulerFromQuaternion(pose[1])[2]
        position = p.getBasePositionAndOrientation(self.pionnerUid)[0][:2]
        for i in range(16):
            target_one = [position[0]+self.lengh*math.cos(math.pi/8*(i)+p.getEulerFromQuaternion(pose[1])[2]-math.pi/6),
                          position[1]+self.lengh*math.sin(math.pi/8*(i)+p.getEulerFromQuaternion(pose[1])[2]-math.pi/6), 0]
            origin_one = [position[0]+self.lengh_robot*math.cos(math.pi/8*(i)+p.getEulerFromQuaternion(pose[1])[2]-math.pi/6),
                          position[1]+self.lengh_robot*math.sin(math.pi/8*(i)+p.getEulerFromQuaternion(pose[1])[2]-math.pi/6), 0]
            target_one_real = [position[0]+math.cos(math.pi/8*(i)+p.getEulerFromQuaternion(pose[1])[2]-math.pi/6),
                               position[1]+math.sin(math.pi/8*(i)+p.getEulerFromQuaternion(pose[1])[2]-math.pi/6), 0]
            p.addUserDebugLine(origin_one, target_one_real,
                               lifeTime=0.01, lineColorRGB=[1, 0, 0])
            target.append(target_one)
            origin.append(origin_one)
        ray_info = p.rayTestBatch(origin, target)
        attached = []
        self.proximity = [0]*16
        for i in range(16):
            if ray_info[i][0] != -1 and ray_info[i][0] != 0:
                attached.append(ray_info[i])
                self.proximity[i] = 1.0

        points = []
        dists = []
        for i in range(len(attached)):
            point = self.transform(pose, attached[i][3])
            points.append(point)
            dist = (point[0]**2+point[1]**2)**0.5
            dists.append(dist)
        # print("origin", origin[0])
        # print("target", target[0])
        # print("robotposition", pose[0])
        # print("Attachedinrobot", points)
        # print("ray_info", ray_info)
        # print("attech", attached)
        # print("dist", dists)
        # print("--------------------------------------------------------------")

        VL, VR = self.braitenberg(dists,self.counter)
        p.setJointMotorControl2(bodyUniqueId=self.pionnerUid,
                                jointIndex=4,
                                controlMode=p.VELOCITY_CONTROL,
                                targetVelocity=VL
                                )

        p.setJointMotorControl2(bodyUniqueId=self.pionnerUid,
                                jointIndex=6,
                                controlMode=p.VELOCITY_CONTROL,
                                targetVelocity=VR)

        # print(ray_info[0][3])

        # ray_test = p.rayTest([0, 0, 0], [5, 0, 0])
        # print(target[4][0]-origin[4][0])
        # print(target[4][1]-origin[4][1])
        # print(ray_info[8])
        # print(ray_test)
        
        self.pose_data = p.getBasePositionAndOrientation(
            bodyUniqueId=self.pionnerUid)[0][0:2]
        p.stepSimulation()
        theta_nex = p.getEulerFromQuaternion(
            p.getBasePositionAndOrientation(self.pionnerUid)[1])[2]
        position_next = p.getBasePositionAndOrientation(self.pionnerUid)[0][0:2]
        theta = theta_nex-theta_pre
        speed=[(position_next[0]-position[0])*self.rate_,(position_next[1]-position[1])*self.rate_]
        if self.counter >1100:
            self.counter=0
        return theta, speed,position
