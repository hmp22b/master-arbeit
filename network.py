# this file is for choosing the computation backend
# numpy is required for both
# see README.md for CUDA installation instructions
# use CUDA if any GPU with CUDA support is available, speedup factor ca. 50-70 (GTX 1070 vs. AMD Ryzen 5 2600)

# comment out the block not used

############### CUDA with pycuda #############
from network_cuda import NetworkTopology as netTop
from network_cuda import NetworkInstance as netInst
NetworkTopology = netTop
NetworkInstance = netInst
##############################################

#################### NumPy ###################
# from network_numpy import NetworkTopology as netTop
# from network_numpy import NetworkInstance as netInst
# NetworkTopology = netTop
# NetworkInstance = netInst
##############################################
