import numpy as np
import matplotlib.pyplot as plt
from params import n_x,n_y


class GCploter:
     def __init__(self):
          plt.ion()
     def plot_active(self,activition):
          plt.clf()
          plt.suptitle('GC active')
          g=plt.subplot(1,1,1)
          g.set_xlabel('X')
          g.set_ylabel('Y')
          X=[]
          Y=[]
          for i in range(len(activition)):
              X.append(i % n_x)
              Y.append(int(i/n_x % n_y))
          g.scatter(X,Y,c=activition,cmap=plt.get_cmap('jet'))
     def stop(self):
          plt.ioff()
          plt.show()
          # ax_plot=self.ax.scatter(X,Y,activition)
          
          
